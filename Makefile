
build:
	python -m build

upload:
	twine upload dist/*

clean:
	rm -rf dist
	rm -rf build
	rm -rf *.egg-info

.PHONY: build upload clean
